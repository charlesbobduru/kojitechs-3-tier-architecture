
terraform {
  required_version = ">1.1"

  backend "s3" {
    bucket         = "kojitech.3.tier.arch.jnj"
    dynamodb_table = "terraform-state-block"
    region         = "us-east-1"
    key            = "path/env"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "Charles"
}

locals {
  vpc_id = aws_vpc.kojitechs_vpc.id
  azs    = data.aws_availability_zones.available.names
}

data "aws_availability_zones" "available" {
  state = "available"
}


# Creating vpc
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "kojitechs_vpc"
  }
}

# Creating internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "gw"
  }
}
#  Creating public subnets
resource "aws_subnet" "public_subnet" {
  count = length(var.public_cidr) # telling terraform calculate the size of public_cidr var

  vpc_id                  = local.vpc_id
  cidr_block              = var.public_cidr[count.index]
  availability_zone       = element(slice(local.azs, 0, 2), count.index) # 
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet_${count.index + 1}"
  }
}

# private subnet 
resource "aws_subnet" "private_subnet" {
  count = length(var.private_cidr) # telling terraform calculate the size of public_cidr var

  vpc_id            = local.vpc_id
  cidr_block        = var.private_cidr[count.index]
  availability_zone = element(slice(local.azs, 0, 2), count.index) # 

  tags = {
    Name = "private_subnet_${count.index + 1}"
  }
}

# database subnet
resource "aws_subnet" "database_subnet" {
  count = length(var.database_cidr) # telling terraform calculate the size of public_cidr var

  vpc_id            = local.vpc_id
  cidr_block        = var.database_cidr[count.index]
  availability_zone = element(slice(local.azs, 0, 2), count.index) # 

  tags = {
    Name = "database_subnet_${count.index + 1}"
  }
}

#Creating public route table
resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "public_route_table"
  }
}

#Creating a route table association to combine pub sub and priv sub/one for each subnet
resource "aws_route_table_association" "pub_association" {
  count          = length(var.public_cidr)
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}


#Creating default route table
resource "aws_default_route_table" "aws_default_route_table" {
  default_route_table_id = aws_vpc.kojitechs_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw.id
  }
}

#Creating nat gateway
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.lb.id                  #would not make sense to have a dynamic address on a NAT device
  subnet_id     = aws_subnet.public_subnet[0].id #deploy in public subnet

  tags = {
    Name = "gw NAT"
  }
  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.gw]
}

#EIP
resource "aws_eip" "lb" {
  vpc        = true
  depends_on = [aws_internet_gateway.gw]
}